[Home](../README.md)


# Table of Contents
- [Preparation](#preparation)
- [Servicing Order for Cash withrawal Sequence Diagram](#servicing-order-for-cash-withrawal-sequence-diagram)
- [Development](#development)

---

## Preparation  

- VScode Extention

  ```
  Extention Pack for JAva
  Spring Boot Extension Pack
  ```

- Gitlab update fork
 gitlab에서 update fork 클릭!
 ![updatefork](images/updatefork.png)

- cli 설정  

  ``` 
  npx k5 setup --file cli-config.json
  ```

- k5 clone   

  ```  
  npx k5 clone -s isw-enablement-project/SVCORD19 -p "gitlab.com"  
  ```  

- Project source code structure  

  | folder/file name | functions |
  |-----------------------|-------------------------------------------------------------|
  | src-design | Solution Design에서 작성한 Desgin 정보|
  | svcord19-application | 실제 서비스를 구성하는 source code |
  | solution.yml | 해당 project의 meta정보|
  | extention-values.yaml | Openshift의 secret과 configMap을 이용하기 위한 env 설정파일 |


- Working folder  
  svcord-application/src/main/java/com/ibm/svcord19  

  | foler name | functions |
  |-------------------------------|----------------------------------------------------|
  | api/v1 | API layer에서 설계한 내용의 로직 작성 |
  | domain/svcord | Domain layer에서 설계한 내용의 로직 작성 |
  | domain/svcord/command | Domain layer의 Command에 설계한 내용의 로직 작성 |
  | domain/svcord/service | Domain layer의 Service에 설계한 내용의 로직 작성 |
  | integration/{IntegrationNamespace} | Integration layer에서 설계한 내용 로직 작성 - 각 namespace별 생성 | 



## Servicing Order for Cash withrawal Sequence Diagram

![servicing order](images/servicingorder.png)  

1. POST /withrawal로 현금인출에 대한 요청을 받음  
2. API layer에서 실제 로직을 관장하는 Service를 호출 함  
3. Service layer에서 위 그림에 보이는 로직을 수행 함
  - integration의 Party Life cycle Management 호출  
  - 사용자 ID의 validation을 요청한 후 결과를 보내 줌  
  - Command를 이용하여 Servicing order가 시작 됨을 생성 함 (새로운 프로시저 시작)
  - 현금결제 요청을 위해 Integration의 Payment Order를 호출 함 - 앞에서 받았던 정보와 현재 서비스의 정보를 같이 전달
  - Payment Order에서 받은 결과를 바탕으로 프로시저를 업데이트 하고 이를 API layer로 보내 줌  
4. Service Layer 의 결과를 API layer의 POST /withdrawal의 결과로 전달 함  


## Development  

### SpringBoot  
Java Spring Boot는 Java 기반 프레임워크를 사용하여 마이크로 서비스 및 웹 앱을 보다 쉽게 만들 수 있는 오픈 소스 도구입니다.   
리소스에 대한 사용을 매우 용이하게 하는 장점이 있어, 우리가 해당 서비스에 DB를 붙이던지, Message queue를 붙이는 부분들이 쉽게 적용 될 수 있고,  
코드 상에서도 이를 쉽게 사용할 수 있게 Annotation들을 제공하고 있습니다.  

- ISW는 java project 생성시 Spring Boot기반의 서비스를 생성해 주며, 이 때 기본적으로 서비스가 배포되어 사용될 때 필요한 configuration은 자동 생성해 줍니다.  
  예를 들어 keyclock 이라든지, Kafka에 대한 configuration이라든지, DB설정 사항등은(MongoDB only)  추가적으로 구성 해 줄 필요가 없습니다.  

- 엔터프라이즈 레벨에서 개발에 대한 표준화를 만들 때 application의 source code 구조 및 naming rules, Configuration의 방식 등에 대해 크게 고민 하지 않아도,  
  generated된 소스코드 위에 필요한 로직만 추가하여 만들 수 있기 때문에 마이크로서비스 세계로 쉽게 갈 수 있게 도와 줍니다.  

- 대체적으로 코딩 순서는 input값이 있는 function을 호출 할 때에는 Builder를 통해 input instance를 생성하고
해당 input을 파라미터로 설정하여 함수를 호출 합니다. 호출 된 함수에서도 다음을 호출할 process가 있다면 동일하게 input을 생성하여 호출 하고, 그 외에는 필요로 하는 로직을 구성 합니다.




  

